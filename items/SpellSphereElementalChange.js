const spellItems = game.items.filter(i => i.type === 'spell' && i.system.type.toLowerCase() === 'divine');
console.log("", { spellItems });

for (const spell of spellItems) {
    if (spell.system.sphere.toLowerCase().startsWith("elemental")) {
        const elements = spell.system.sphere.split(',').map(i => i.trim());
        let newSphere = elements[0];
        if (elements.length > 1) {
            for (let i = 1; i < elements.length; i++) {
                if (['air', 'earth', 'fire', 'water'].includes(elements[i].toLowerCase())) {
                    console.log(`${spell.system.sphere.trim()}: Found ${elements[i]} to create}`, { elements });
                    newSphere += `,Elemental-${elements[i]}`;
                } else {
                    const insertText = elements[i] = elements[i].charAt(0).toUpperCase() + elements[i].slice(1);
                    newSphere += `,${insertText}`;
                }
            }
        }
        console.log(`Completed sphere entry: ${newSphere}`);
        spell.update({ 'system.sphere': newSphere });
    } else {
        // no elemental entries
    }
}// for