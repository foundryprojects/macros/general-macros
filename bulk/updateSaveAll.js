// Define a function to process effects of an object (either actor or item)
async function processEffects(obj) {
    // console.log('processEffects', obj.name, { obj });
    for (const effect of obj.effects) {
        for (const [chindex, change] of effect.changes?.entries()) {
            await updateEffect(effect, change, chindex);
        }
    }

    async function actionHelper(obj, index, chindex, change) {
        let testDetails = undefined;
        try {
            testDetails = JSON.parse(change.value);
        } catch (err) {}
        if (testDetails?.hasOwnProperty('formula')) return;

        console.log('processEffects actions fixing ', obj.name, { obj, index, change });
        const opts = change.value
            .toLowerCase()
            .split(' ')
            .map((text) => text.trim());

        const details = {
            formula: opts[0] || 1,
            properties: opts[1] || '',
        };

        const newChange = {
            key: change.key,
            mode: 0,
            value: JSON.stringify(details),
        };

        const actionBundle = duplicate(obj.system.actions);
        actionBundle[index].effect.changes[chindex] = { ...newChange };
        await obj.update({ 'system.actions': actionBundle });
    }

    if (obj.system?.actions)
        for (const [index, action] of obj.system?.actions?.entries()) {
            // console.log('processEffects actions ', action.name, { action });
            if (action.type === 'effect') {
                for (const [chindex, change] of action.effect?.changes?.entries()) {
                    if (
                        change.key.toLowerCase() === 'system.mods.saves.all' ||
                        change.key.toLowerCase() === 'aura.system.mods.saves.all'
                    ) {
                        console.log('processEffects checking action ', action.name, { action });
                        await actionHelper(obj, index, chindex, change);
                    }
                }
            }
        }
    // actors can have sub-items
    if (obj.items) {
        for (const item of obj.items) {
            processEffects(item);
        }
    }
}

function updateEffect(effect, change, index) {
    async function effectChangeHelper(effect, newChange, chindex) {
        const changeBundle = duplicate(effect.changes);
        changeBundle[chindex] = { ...newChange };
        await effect.update({ changes: changeBundle });
    }

    if (change.key.toLowerCase() === 'system.mods.saves.all' || change.key.toLowerCase() === 'aura.system.mods.saves.all') {
        // { name: "system.mods.saves.poison", mode: 0, value: '{"formula": "1", "properties": ""}' },
        let testDetails = undefined;
        try {
            testDetails = JSON.parse(change.value);
        } catch (err) {}
        if (testDetails?.hasOwnProperty('formula')) return;

        console.log('processEffects updateEffect fixing ', effect.name, { effect, change, index });
        const opts = change.value
            .toLowerCase()
            .split(' ')
            .map((text) => text.trim());
        const details = {
            formula: opts[0] || 1,
            properties: opts[1] || '',
        };
        const newChange = {
            key: change.key,
            mode: 0,
            value: JSON.stringify(details),
        };

        effectChangeHelper(effect, newChange, index);
    }
}

for (const actor of game.actors) {
    await processEffects(actor);
}
for (const item of game.items) {
    await processEffects(item);
}
