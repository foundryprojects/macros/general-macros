/*
 * Bulk replace asset references
 */
(async () => {
  Dialog.prompt({
    title: 'Bulk replace asset references',
    content:
      '<p>This tool will search for asset references <strong>within your world</strong> that start with the provided value and replace that portion with the other provided value.</p>' +
      '<p><label>Find (case sensitive): <input type="text" name="find" placeholder="some/original/path/"></label></p>' +
      '<p><label>Replace (case sensitive): <input type="text" name="replace" placeholder="your/new/path/"></label></p>' +
      '<hr><p><label><input type="checkbox"> Save changes?</label></p><p>Leaving this unchecked will operate in a "dry run" mode, where changes are only output to the console (F12).</p><hr>',
    label: 'Bulk replace',
    callback: async (html) => {
      const find = html.find('input[name="find"]').val();
      const replace = html.find('input[name="replace"]').val();
      const dryRun = !html.find('input[type="checkbox"]')[0].checked;
      if (!find || !replace) {
        ui.notifications.warn('Please provide both a find and replace value.', { permanent: true });
        console.warn('Bulk replace asset references: Please provide both a find and replace value.');
        return;
      }

      const documentTypes = CONST.COMPENDIUM_DOCUMENT_TYPES || CONST.COMPENDIUM_ENTITY_TYPES || [];
      for (const documentType of documentTypes) {
        const collectionName = CONFIG[documentType]?.documentClass?.collectionName;
        if (!collectionName) {
          console.warn(`Bulk replace asset references: No collection name for ${documentType}`);
          continue;
        }

        const collection = game[collectionName];
        if (!collection) {
          continue;
        }

        console.groupCollapsed(`Bulk replace asset references: ${collectionName}`);
        for (const document of collection) {
          const original = document.toJSON();
          const replacementData = JSON.stringify(original).replaceAll(`"${find}`, `"${replace}`);
          if (original === replacementData) {
            continue;
          }

          const diff = diffObject(original, JSON.parse(replacementData));
          if (isEmpty(diff)) {
            continue;
          }

          console.log(`Bulk replace asset references: Updating ${documentType} ${document.id} ${document.name}`);
          console.table(diff);
          if (!dryRun) {
            await document.update(diff);
          }
        }

        console.groupEnd();
      }

      ui.notifications.info('Bulk replace asset references complete. Check the console (F12) for full details.', { permanent: true });
      console.log('Bulk replace asset references complete.');
    },
  });
})();