/**
 * bulk delete entries entire journal pages over a level 
 */

const journalId = "At3eW7LbIxiKDMQJ";
const journal = game.journal.get(journalId);
const pages = journal.pages;
let deleteThese = [];
pages.forEach((p) => {
    if (p.name.match(/-- Magical Item$/i)) {
        console.log("matched", { p })
        deleteThese.push(p.id);
    }
});
journal.deleteEmbeddedDocuments("JournalEntryPage", deleteThese);