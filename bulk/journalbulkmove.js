/**
 * bulk move entries entire journal pages over a level 
 */

const journalId = "4HXwVGljj1szE73m";
const newJournalName = "Potions"
let moveThese = [];
let deleteThese = [];
game.journal.get(journalId).pages.forEach((p) => {
    if (p.name.match(/-- Potion$/i)) {

        console.log("matched", { p })

        const textPage = p.toObject();
        textPage.title.show = true;
        textPage.title.level = 1;
        moveThese.push(textPage);
        deleteThese.push(p.id);
    }
});
console.log("moveThese", { moveThese })
const pages = moveThese;
JournalEntry.implementation.create({
    pages,
    name: newJournalName,
});
game.journal.get(journalId).deleteEmbeddedDocuments("JournalEntryPage", deleteThese);
