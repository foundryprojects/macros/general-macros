/**
 * 
 * copy all the pages in sourceJournal to the targetJournal
 * 
 */
const sourceJournalId = "3mD3qbma9pQChhGZ";
const targetJournalId = "K6ZCZiqibes5uVEI";
const source = game.journal.get(sourceJournalId);
const target = game.journal.get(targetJournalId);
let i = 0;
console.log("sort start = ", { target, source }, i);
target.pages.forEach((p) => {
    if (i < p.sort) i = p.sort;
});
console.log("sort start = ", i);
const _pages = () => {
    const pages = [];
    source.pages.forEach((entry) => {
        i++;
        // Preserve sort order in the folder.
        let sort = (i + 1) * 200_000;
        const textPage = entry.type === "text" ? entry.toObject() : undefined;
        const imagePage = entry.type === "image" ? entry.toObject() : undefined;
        if (textPage) {
            textPage.title.show = true;
            textPage.sort = sort;
            pages.push(textPage);
            sort -= 100_000;
        }
        if (imagePage) {
            imagePage.sort = sort;
            pages.push(imagePage);
        }
    });
    return pages;
}


let mypages = _pages();
target.createEmbeddedDocuments("JournalEntryPage", mypages);