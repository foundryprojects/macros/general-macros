/**
 * This will convert the v9 style of journals to v10.
 * Meaning the single journal page style and put all
 * pages from a folder into a single journal with sub-pages.
 *
 *
 */
let jEntries = [];

const folders = game.folders.filter((f) => f.type === 'JournalEntry');
folders.forEach((folder) => {
    let folderId = null;
    let folderName = folder.name;
    const sort = folder.sorting === 'm' ? SidebarDirectory._sortStandard : SidebarDirectory._sortAlphabetical;
    const depth = folder.depth;
    let level = 1;
    switch (depth) {
        // case 2:
        //     folderId = folder.folder?.id;
        //     folderName = folder?.name;
        //     break;
        // case 3:
        //     folderId = folder.folder?.folder?.id;
        //     folderName = folder.folder?.name;
        //     level = 2;
        //     break;
        default:
            folderId = folder?.id;
            folderName = folder?.name;
            break;
    }

    console.log('journal v10 conversion:', { depth }, folder.name);

    const contents = folder.contents.sort(sort);

    const pages = contents.flatMap((entry, i) => {
        const pages = [];
        // Preserve sort order in the folder.
        let sort = (i + 1) * 200_000;
        const textPage = entry.pages.find((p) => p.type === 'text')?.toObject();
        const imagePage = entry.pages.find((p) => p.type === 'image')?.toObject();
        if (textPage) {
            textPage.title.show = true;
            textPage.title.level = level;
            textPage.sort = sort;
            pages.push(textPage);
            sort -= 100_000;
        }
        if (imagePage) {
            imagePage.sort = sort;
            pages.push(imagePage);
        }

        console.log(`pages:`, { pages });

        return pages;
    });

    const allPages = pages;
    console.log(`allPages:`, { pages });

    if (allPages.length && folderName) {
        console.log(`${folderName} into:`, { allPages });
        if (jEntries[folderName]) jEntries[folderName].pages = jEntries[folderName].pages.concat(allPages);
        else
            jEntries[folderName] = {
                name: folderName,
                pages: allPages,
                folder: folderId,
            };
    }
});
console.log('journal v10 conversion:', { jEntries });
for (const jindex in jEntries) {
    const journal = jEntries[jindex];
    console.log('journal v10 conversion:', { journal });
    if (journal?.name) {
        const pages = journal.pages;
        JournalEntry.implementation.create({
            pages,
            name: journal.name,
            folder: journal.folderId,
        });
    }
}
