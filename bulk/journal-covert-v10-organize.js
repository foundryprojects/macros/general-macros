/**
 * Convert journal folders to journal entries with pages. Typical from
 * v9 journals to v10.
 *
 * @returns
 */
async function convertJournalFoldersToPages() {
    if (!game.user.isGM) return;

    // Object to hold the gathered journal data
    const journalData = {};

    // Function to gather journal data recursively
    async function gatherJournalData(folder, parentKey = null) {
        // Log the folder being processed
        console.log('convertJournalFoldersToPages gatherJournalData ', folder.name, folder.depth, { folder, parentKey });

        // Determine folder name
        const folderName = folder.name || 'Unnamed Folder';

        // Create a unique key for the folder
        const folderKey = `${folderName}_${folder.id}`;

        // Determine effective parent based on folder depth
        let effectiveParentKey = folderKey;
        if (folder.depth == 3) effectiveParentKey = parentKey;
        if (folder.depth == 4) effectiveParentKey = `${folder.ancestors[1].name}_${folder.ancestors[1].id}`;

        // Initialize or update journalData for the effective parent
        if (!journalData[effectiveParentKey]) {
            journalData[effectiveParentKey] = {
                name: folderName,
                pages: [],
                parent: effectiveParentKey,
            };
        }

        // Retrieve journals in the folder
        const journalEntries = folder?.contents;
        // Process each journal entry in the folder
        if (journalEntries?.length) {
            const jEntries = journalEntries.sort((a, b) => a.sort - b.sort);
            let firstRecord = true;
            for (const journal of jEntries) {
                const jPages = journal.pages;
                const arrayFromCollection = Array.from(jPages.values());
                // Sort the array
                const sortedArray = arrayFromCollection.sort((a, b) => a.sort - b.sort);
                for (const page of sortedArray) {
                    const pageObj = page.toObject();

                    if (!firstRecord && folder.depth == 3) {
                        pageObj.title.level = 2;
                    }
                    if (!firstRecord && folder.depth == 4) {
                        pageObj.title.level = 3;
                    }
                    firstRecord = false;
                    journalData[effectiveParentKey].pages.push({
                        ...pageObj,
                        sort: journal.sort,
                        sorting: journal.sorting,
                    });
                }
            }
        }

        // Process child folders
        const childFolders = folder.children;
        console.log('convertJournalFoldersToPages ', { childFolders });

        if (childFolders?.length) {
            for (const childFolder of childFolders) {
                console.log('convertJournalFoldersToPages ', { folderKey, childFolder });
                await gatherJournalData(childFolder.folder, folderKey);
            }
        }
    }

    // Function to create journal entries based on the gathered data
    async function createJournalEntries() {
        // Create a new root folder for all the new Journal Entries
        const newRootFolder = await Folder.create({
            name: 'Converted Journals',
            type: 'JournalEntry',
            sorting: 'm',
            parent: null,
        });

        for (const [key, data] of Object.entries(journalData)) {
            console.log('convertJournalFoldersToPages Creating Journal Entry: ', data.name, { data });
            await JournalEntry.implementation.create({
                name: data.name,
                pages: data.pages,
                folder: newRootFolder.id,
                sort: data.sort,
                sorting: data.sorting,
            });
        }
    }

    // Get root-level journal folders
    const rootFolders = game.folders.filter((f) => f.type === 'JournalEntry' && f.folder === null);
    console.log('convertJournalFoldersToPages ', { rootFolders });

    // Process each root-level folder
    for (const folder of rootFolders) {
        await gatherJournalData(folder);
    }

    // Create journal entries after all data has been gathered
    await createJournalEntries();
}

convertJournalFoldersToPages();
