/**
 * This is used to bulk fix links in Tables that a link to a compendium
 * but should point to local/world files.
 * 
 */
ui.notifications.info(`STARTING table fixes...`, { permanent: true });
for (const table of game.tables) {
    console.log(`Fixing Table ${table.name}`);
    for (const result of table.results) {
        // console.log(`Updating Item ${table.name}`, { table, result });
        // console.log("result.text", result.text);
        // is this a item?
        let refObject = game.items.getName(result.text);

        //is this a table?
        if (!refObject) {
            // console.log("refObject checking tables", { refObject });
            refObject = game.tables.getName(result.text);
        }
        //is this a actor?
        if (!refObject) {
            // console.log("refObject checking actors", { refObject });
            refObject = game.actors.getName(result.text);
        }
        //is this a journal?
        if (!refObject) {
            // console.log("refObject checking journal", { refObject });
            refObject = game.journal.getName(result.text);
        }
        // console.log("refObject", { refObject });
        // if (!refObject) console.log("!!!refObject===========================>", { result }, result.text);
        if (refObject) {
            await result.update({ 'documentCollection': refObject.documentName, 'type': 1, 'img': refObject.img, 'documentId': refObject.id });
        }
        else
            await result.update({ '-=documentCollection': null, 'type': 0, '-=documentId': null });

        // await tableUpdate(table);
    };
};
console.log("Done with table corrections.");
ui.notifications.info(`COMPLETED table fixes...`, { permanent: true });