/**
 *
 * Test actors for MR and add MR effect.
 *
 **/
console.warn('Starting run...');
// Iterate over all actors in the game
for (const actor of game.actors.contents) {
    // Check if the actor has the property 'system.magicresist'
    let magicResist = getProperty(actor.system, 'magicresist');
    if (typeof magicResist === 'string' && magicResist.endsWith('%')) {
        // Extract the numeric value from the string (e.g., '50%' becomes 50)
        let resistValue = parseInt(magicResist);
        if (!isNaN(resistValue)) {
            console.warn(`Adding MR effect on ${actor.name} of value ${resistValue}`);
            // Define the active effect data
            let effectData = {
                label: 'Magic Resist',
                icon: 'icons/svg/shield.svg', // Change the icon as needed
                changes: [
                    {
                        key: 'system.mods.magic.resist',
                        mode: CONST.ACTIVE_EFFECT_MODES.ADD,
                        value: resistValue,
                    },
                ],
                duration: { seconds: null, rounds: null, turns: null }, // No duration
                origin: actor.uuid, // Link the effect to the actor
            };

            // Apply the active effect to the actor
            actor.createEmbeddedDocuments('ActiveEffect', [effectData]);
            const magicResistSkill = actor.items.find((i) => i.type === 'skill' && i.name === 'Magic Resistance');
            if (magicResistSkill) {
                console.warn(`...found Magic Resist item, updating target to formula (@mods.magic.resist)`);
                // Update the skill item's target property
                let updateData = {
                    _id: magicResistSkill.id,
                    'system.features.target': '(@mods.magic.resist)',
                };

                // Apply the update to the actor
                actor.updateEmbeddedDocuments('Item', [updateData]);
            }
        }
    }
}

console.warn('DONE');
