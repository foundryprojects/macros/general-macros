/**
 * This will move all 'Item' and 'RollTable' type 1 result entries to type 2 compendium link
 *
 */
const itemCompendiumPath = 'adnd2e-dmg.items';
const rollTableCompendiumPath = 'adnd2e-dmg.rolletables';
ui.notifications.info(`Starting compendium fix of tables`, { permanent: true });
console.log(`...Starting compendium fix of tables`);
for (const table of game.tables) {
    const resultsBundle = duplicate(table.results);
    let changed = false;
    for (const result of resultsBundle) {
        if (result.documentCollection == 'Item' && result.type == 1) {
            changed = true;
            result.type = 2;
            result.documentCollection = itemCompendiumPath;
            console.log(`Fixing Item result ${result.text}`);
        } else if (result.documentCollection == 'RollTable' && result.type == 1) {
            changed = true;
            result.type = 2;
            result.documentCollection = rollTableCompendiumPath;
            console.log(`Fixing RollTable result ${result.text}`);
        }
    }
    if (changed) await table.update({ results: resultsBundle });
}
console.log(`...DONE with compendium fix of tables`);
ui.notifications.info(`DONE with compendium fix of tables`, { permanent: true });
