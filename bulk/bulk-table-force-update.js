
async function tableUpdate(table) {
    // console.log(`Table update ${table.name}`, { table })
    console.log(`Table update ${table.name}`)
    const expanded = duplicate(table.results);
    // console.log(`Table update ${table.name} 0`, { expanded })
    for (let r of expanded) {
        // console.log(`Table update 2 ${table.name}`, { r })
        switch (r.type) {
            // Document results
            case CONST.TABLE_RESULT_TYPES.DOCUMENT:
                const collection = game.collections.get(r.documentCollection);
                if (!collection) continue;

                // Get the original document, if the name still matches - take no action
                const original = r.documentId ? collection.get(r.documentId) : null;
                if (original && (original.name === r.text)) continue;

                // Otherwise, find the document by ID or name (ID preferred)
                const doc = collection.find(e => (e.id === r.text) || (e.name === r.text)) || null;
                r.documentId = doc?.id ?? null;
                r.text = doc?.name ?? null;
                r.img = doc?.img ?? null;
                r.img = doc?.thumb || doc?.img || null;
                break;

            // Compendium results
            case CONST.TABLE_RESULT_TYPES.COMPENDIUM:
                const pack = game.packs.get(r.documentCollection);
                if (pack) {

                    // Get the original entry, if the name still matches - take no action
                    const original = pack.index.get(r.documentId) || null;
                    if (original && (original.name === r.text)) continue;

                    // Otherwise, find the document by ID or name (ID preferred)
                    const doc = pack.index.find(i => (i._id === r.text) || (i.name === r.text)) || null;
                    r.documentId = doc?._id || null;
                    r.text = doc?.name || null;
                    r.img = doc?.thumb || doc?.img || null;
                }
                break;

            // Plain text results
            default:
                r.type = 0;
                r.documentCollection = null;
                r.documentId = null;
        }
    }

    // Update the object
    // console.log(`Table update DONE ${table.name}`, { expanded })
    await table.update({ 'results': expanded });
}


function tableUpdate() {
    for (const table of game.tables) {
        tableUpdate(table)
    }
}
