/**
 * shift entire journal pages over a level 
 */

const journalId = "K6ZCZiqibes5uVEI";

game.journal.get(journalId).pages.forEach((p) => { p.update({ 'title.level': 2 }) });