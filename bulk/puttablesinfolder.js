
ui.notifications.info(`Starting organization of tables`, { permanent: true });
for (const table of game.tables) {

    /**
       * 
       * @param {*} name 
       * @param {*} type 
       * @param {*} sortMethod 
       * @returns 
       */
    async function createFolder(name, type, sortMethod = 'a', forceNew = false) {
        const folderData = {
            name: name,
            type: type,
            sorting: sortMethod,
            sort: 0,
        };
        let folder = !forceNew ? game.folders.find(p => p.name === name) : null;
        if (!folder) folder = await Folder.implementation.create(folderData);
        return folder;
    }

    const folder = await createFolder('folder', 'RollTable');
    await table.update({ 'folder': folder.id });
}


ui.notifications.info(`DONE with organization of tables`, { permanent: true });