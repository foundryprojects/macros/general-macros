const moduleName = "osric-compendium";
/**
 * 
 * replace [world. with [moduleName.
 * @param {*} text 
 * @returns 
 */
function fixPaths(text) {
    text = text.replace(new RegExp(/\[world\./, "g"), `[${moduleName}.`);
    return text;
}

console.log("Starting pathing changes...")
const itemPacks = game.packs.filter(p => { return p.metadata.type === 'Item' });
const actorPacks = game.packs.filter(p => { return p.metadata.type === 'Actor' });
const journalPacks = game.packs.filter(p => { return p.metadata.type === 'JournalEntry' });


for (const pack of itemPacks) {
    const items = await pack.getIndex();
    for (const item of items) {
        console.log("item: ", { item }, item.name)
        const matchedItem = await pack.getDocument(item._id);
        console.log("matchedItem: ", { matchedItem }, matchedItem.name)
        await matchedItem.update({ 'data.description': fixPaths(matchedItem.data.data.description) })
        await matchedItem.update({ 'data.dmonlytext': fixPaths(matchedItem.data.data.dmonlytext) })
    }
}

for (const pack of actorPacks) {
    const actors = await pack.getIndex();
    for (const actor of actors) {
        console.log("item: ", { actor }, actor.name)
        const matchedActor = await pack.getDocument(actor._id);
        console.log("matchedActor: ", { matchedActor }, matchedActor.name)
        matchedActor.update({ 'data.details.biography.value': fixPaths(matchedActor.data.data.details.biography.value) });
    }
}

for (const pack of journalPacks) {
    const journals = await pack.getIndex();
    for (const journal of journals) {
        console.log("journal: ", { journal }, journal.name)
        const matchedJournal = await pack.getDocument(journal._id);
        console.log("matchedJournal: ", { matchedJournal }, matchedJournal.name)
        matchedJournal.update({ 'data.content': fixPaths(matchedJournal.data.content) })
    }
}

console.log("Done!")
